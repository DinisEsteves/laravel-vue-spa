/*
|-------------------------------------------------
|The main app imports are going to be stored here
|-------------------------------------------------
*/
import Vue from 'vue';
import VueRouter from 'vue-router';
/*
|-------------------------------------------------
|Import Route file that is going to be used below
|-------------------------------------------------
*/
import routes from './routes';


Vue.use(VueRouter);


let app = new Vue({
    el: '#app',
    router: new VueRouter(routes),
});
