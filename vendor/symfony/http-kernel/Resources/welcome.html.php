<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Laravel!</title>

</head>
<body>
<div id="app">
<router-view></router-view>
</div>
<script src="/js/app.js"></script>
</body>
</html>
